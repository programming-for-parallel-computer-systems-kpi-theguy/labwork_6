Laboratory work #6 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Monitors, synchronized blocks and shared variable mechanism, that was made for programming language - Java.
Programming language: Java.
Used technologies: Java programming language with it's OOP principle gives opportunity to create sequential control of the stored variables inside of the objects by using the keyword - synchronized. Events between threads works using build-in functions of the language.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 6 threads, fourth one outputs the result.