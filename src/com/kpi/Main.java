//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #6------------------------------------
//-------Java. Monitors, synchronized blocks, shared variables--------------
//--------------------------------------------------------------------------
//-------Task: MA = min(Z) * MO + (B * C) * (MS*MT)-------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 02.05.2018--------------------------------------------------
//--------------------------------------------------------------------------

package com.kpi;

public class Main {

    private static int N = 6;
    private static int P = 6;

    public static int[] B = new int[N];
    public static int[] C = new int[N];
    public static int[] Z = new int[N];
    public static int[][] MA = new int[N][N];
    public static int[][] MO = new int[N][N];
    public static int[][] MT = new int[N][N];
    public static volatile int[][] MS = new int[N][N];

    public static void main(String[] args) throws InterruptedException {
        Data data = new Data(N, P);
        Monitor_Control monitor = new Monitor_Control(P);
        Task[] tasks = new Task[P];

        System.out.println("Labwork #6 started");
        for(int i = 0; i < P; i++){
            tasks[i] = new Task(monitor,data,i+1);
            tasks[i].start();
        }

        tasks[3].join();

        System.out.println("Labwork #6 finished");
    }
}
