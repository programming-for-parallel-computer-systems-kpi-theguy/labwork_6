//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #6------------------------------------
//-------Java. Monitors, synchronized blocks, shared variables--------------
//--------------------------------------------------------------------------
//-------Task: MA = min(Z) * MO + (B * C) * (MS*MT)-------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 02.05.2018--------------------------------------------------
//--------------------------------------------------------------------------

package com.kpi;

public class Monitor_Control {
    private int a = 1;
    private int b = 0;
    private int F1 = 0;
    private int F2 = 0;
    private int F3 = 0;

    private int P;

    public Monitor_Control(int P){
        this.P = P;
    }

    public synchronized void set_Min(int a){
        this.a = Math.min(this.a, a);
    }

    public synchronized int get_a(){
        return a;
    }

    public synchronized void set_Scalar_Res(int a){
        this.b += a;
    }

    public synchronized int get_b(){
        return b;
    }

    public synchronized void signal_Input(){
        F1++;
        if(F1 >= 4)
            notifyAll();
    }

    public synchronized void wait_Input(){
        try {
            if(F1 < 4)
                wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void signal_Calculation_Scalr(){
        F2++;
        if(F2 >= P)
            notifyAll();
    }

    public synchronized void wait_Calculation_Scalr(){
        try {
            if(F2 < P)
                wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void signal_Calculation(){
        F3++;
        if(F3 >= P)
            notifyAll();
    }

    public synchronized void wait_Calculation(){
        try {
            if(F3 < P)
                wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}