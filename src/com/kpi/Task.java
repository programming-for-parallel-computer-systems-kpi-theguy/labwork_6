//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #6------------------------------------
//-------Java. Monitors, synchronized blocks, shared variables--------------
//--------------------------------------------------------------------------
//-------Task: MA = min(Z) * MO + (B * C) * (MS*MT)-------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 02.05.2018--------------------------------------------------
//--------------------------------------------------------------------------

package com.kpi;

public class Task extends Thread{

    Monitor_Control monitor;

    Data data;

    int Tid;

    public Task(Monitor_Control control, Data data, int Tid){
        this.monitor = control;
        this.data = data;
        this.Tid = Tid;
    }

    @Override
    public void run(){
        int ai, bi;
        System.out.println("T"+Tid+" started");
        //1.Input MT, MO in T1
        if(Tid == 1){
            data.input_Matrix(Main.MT);
            data.input_Matrix(Main.MO);

            //2.Signal about end of input in T1
            monitor.signal_Input();
        }

        //1.Input MS in T2
        if(Tid == 2){
            data.input_Matrix(Main.MS);

            //2.Signal about end of input in T2
            monitor.signal_Input();
        }

        //1.Input C, Z in T3
        if(Tid == 3){
            data.input_Vector(Main.C);
            data.input_Vector(Main.Z);

            //2.Signal about end of input in T3
            monitor.signal_Input();
        }

        //1.Input B in T4
        if(Tid == 4){
            data.input_Vector(Main.B);

            //2.Signal about end of input in T4
            monitor.signal_Input();
        }

        //3.Wait for data input in T1-T4
        monitor.wait_Input();

        //4.Calculation of local min
        ai = data.min_Vector(Main.Z,Tid);

        //5.Calculation of the global min
        monitor.set_Min(ai);

        //6.Calculation of the local scalar multiply
        bi = data.scalar_Multiply_Vectors(Main.B,Main.C,Tid);

        //7.Calculation of the global scalar multiply
        monitor.set_Scalar_Res(bi);

        //8.Signal about end of the scalar multiply
        monitor.signal_Calculation_Scalr();

        //9.Wait for the end of the scalar multiplies in other tasks
        monitor.wait_Calculation_Scalr();

        //10.Copying critical variables, except volatile
        ai = monitor.get_a();
        bi = monitor.get_b();

        //11.Calculation of the function
        data.sum_Matrixes(data.multiply_Matrix_Integer(Main.MO,ai,Tid),data.multiply_Matrix_Integer(data.multiply_Matrixes(Main.MT,Main.MS,Tid),bi,Tid),Main.MA,Tid);

        //12.Signal about end of calculation of function
        monitor.signal_Calculation();

        if(Tid == 4){
            //13.Wait for the end of calculation in all tasks
            monitor.wait_Calculation();

            //14.Output the result
            data.output_Matrix(Main.MA);
        }

        System.out.println("T"+Tid+" finished");
    }
}
