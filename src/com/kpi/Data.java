//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #6------------------------------------
//-------Java. Monitors, synchronized blocks, shared variables--------------
//--------------------------------------------------------------------------
//-------Task: MA = min(Z) * MO + (B * C) * (MS*MT)-------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 02.05.2018--------------------------------------------------
//--------------------------------------------------------------------------

package com.kpi;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class Data {
    private int N;
    private int P;
    private int H;

    public Data(int N, int P){
        this.N = N;
        this.P = P;
        H = N / P;
    }

    public void input_Vector(int[] A){
        for(int i = 0; i < N; i++){
            A[i] = 1;
        }
    }

    public void input_Matrix(int[][] MA){
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                MA[i][j] = 1;
            }
        }
    }

    public void output_Matrix(int[][] MA){
        if(N <= 10){
            System.out.println();
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(MA[i][j]+" ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    public int[][] multiply_Matrixes(int[][] MA, int[][] MB, int k){
        int cell;
        int[][] result = new int[N][N];
        for(int i = H * (k - 1); i < H * k; i++){
            for(int j = 0; j < N; j++){
                cell = 0;
                for(int l = 0; l < N; l++){
                    cell += MA[i][l] * MB[l][j];
                }
                result[i][j] = cell;
            }
        }
        return result;
    }

    public int[][] multiply_Matrix_Integer(int[][] MA, int a, int k){
        int[][] result = new int[N][N];
        for(int i = H * (k - 1); i < H * k; i++){
            for(int j = 0; j < N; j++){
                result[i][j] = a * MA[i][j];
            }
        }
        return result;
    }

    public void sum_Matrixes(int[][] MA, int[][] MB, int[][] MC, int k){
        for(int i = H * (k - 1); i < H * k; i++) {
            for (int j = 0; j < N; j++) {
                MC[i][j] = MA[i][j] + MB[i][j];
            }
        }
    }

    public int min_Vector(int[] A, int k){
        return new ForkJoinPool().invoke(new VectorMin(A,H * (k - 1),H*k));
    }
    protected class VectorMin extends RecursiveTask<Integer>{

        private final int THRESHOLD = P * 10;
        private int[] A;
        private int start;
        private int end;

        public VectorMin(int[] A, int start, int end){
            this.A = A;
            this.start = start;
            this.end = end;
        }

        @Override
        protected Integer compute() {

            int length = start - end;

            if(length < THRESHOLD)
                return computeDirectly(A);

            int middle = (start + end) / 2;

            VectorMin left = new VectorMin(A,start,middle);
            left.fork();
            VectorMin right = new VectorMin(A,middle + 1, end);

            return Math.min(right.compute(), left.join());
        }

        private int computeDirectly(int[] A){
            int result = A[start];
            for(int i = start; i < end; i++) {
                if(A[i] < result){
                    result = A[i];
                }
            }
            return result;
        }
    }

    public int scalar_Multiply_Vectors(int[] A, int[] B, int k){
        return new ForkJoinPool().invoke(new VectorsScalar(A,B,H * (k - 1),H*k));
    }

    public class VectorsScalar extends RecursiveTask<Integer>{

        private final int THRESHOLD = P * 10;
        private int[] A;
        private int[] B;
        private int start;
        private int end;

        public VectorsScalar(int[] A, int[] B, int start, int end){
            this.A = A;
            this.B = B;
            this.start = start;
            this.end = end;
        }

        @Override
        protected Integer compute() {

            int length = start - end;

            if(length < THRESHOLD)
                return computeDirectly(A, B);

            int middle = (start + end) / 2;

            VectorsScalar left = new VectorsScalar(A,B,start,middle);
            left.fork();
            VectorsScalar right = new VectorsScalar(A,B,middle+1,end);

            return Math.addExact(right.compute(), left.join());
        }

        private int computeDirectly(int[] A, int[] B){
            int result = 0;
            for(int i = start; i < end; i++) {
                result += A[i] * B[i];
            }
            return result;
        }


    }
}
